import parser from "http-string-parser";
import buildHttpResponse from 'raw-http-response';

const parseHttpRequest = (request) => {
    const arr = request.split("\r\n");
    const parsed = arr.reduce((v,f,i) => {
        if(i === 0){
            const indexOfK = f.indexOf("/");
            const rest = replaceFirstChar(f.substr(indexOfK));
            const indexOfHttp = f.indexOf("HTTP");
            const url = f.slice(indexOfK,indexOfHttp-1)
            const str = f.slice(0,indexOfK);
            return ({[str]:rest, method:str, url});
        }
        if(f === "") return v;
        if(i === arr.length - 1 && f.includes("{")){
            return ({...v, body:JSON.stringify(f)});
        }
        const indexOfK = f.indexOf(":");
        const rest = replaceFirstChar(f.substr(indexOfK+1));
        const str = f.slice(0,indexOfK);
        return ({...v, [str]:rest});
    }, {});
    return parsed;
}

const replaceFirstChar = string => {
    if(string.charAt(0) === " ") return string.substr(1);
    return string;
}

const res = buildHttpResponse({
    ...req,
    "Content-Length":msg.length,
    headers: {
        ...req.headers,

    },
    status:200,
    body: Buffer.from(msg)
})
