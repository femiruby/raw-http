# Welcome to @feminpm/raw-http 👋
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)

> Useful on TCP Servers. Able to receive raw http requests to build raw http response.

### 🏠 [Homepage](https://bitbucket.org/femiruby/raw-http#readme)

## Usage

```sh
todo: documentation to be updated
```

## Author

👤 **Femi Adeniyi**

* Website: https://about.me/femiadeniyi
* Github: [@femiruby](https://github.com/femiruby)
* LinkedIn: [@femiadeniyi](https://linkedin.com/in/femiadeniyi)

## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_